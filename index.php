<?php

/* Домашнее задание к лекции 5.1 «Менеджер зависимостей Composer»

Реализовать сервис по определению широты и долготы по адресу.
Внешний вид на ваше усмотрение. Опубликовать сервис на своем учебном хостинге. Предоставить ссылку на рабочий пример и ссылку на исходный код проекта (github, bitbucket, cloud9).

В исходном коде обязательно должны присутствовать файлы composer.json, composer.lock, .gitignore.
    - Установить Composer.
    - Установить библиотеку yandex/geo.
    - Создать форму с полем “Адрес” и кнопкой “Найти”.
    - Используя API Yandex, вывести все варианты широты и долготы.

Дополнительное задание:
    - Показать точку на карте, используя JavaScript-библиотеку Яндекс.Карты https://tech.yandex.ru/maps/jsapi/
    - Если API Yandex выдало несколько вариантов (что бывает, если указать неточный адрес), вывести эти варианты в виде ссылок.
        При нажатии на любую из них показать на карте именно эту точку.
*/

require_once 'lib/function.php';

if (array_key_exists('findAddress', $_POST)) {
    $address = getParamPost('address');
    $_SESSION['address'] = $address;
} else {
    $address = getParamSession('address');
}

$lat = getParamGet('lat');
$lon = getParamGet('lon');

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>PHP-19. Task 5.1</title>
    <style>
        .result {
            width: 50%;
            float: left;
        }

        .map {
            float: left;
            margin-left: 1%;
            width: 48%;
            height: 400px;
            border: 1px solid #000;
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
            width: 100%;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: block;
        }

    </style>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<form method="post">
    <input type="text" name="address" placeholder="Адрес" value="<?= $address ?>">
    <input type="submit" name="findAddress" value="Найти">
</form>
<div class="result">
    <? if (!empty($address)): ?>
    <table>
        <tr>
            <th>Адрес</th>
            <th>Широта</th>
            <th>Долгота</th>
            <th>Тип</th>
        </tr>
        <? foreach (findAddress($address)->getList() as $item): ?>
            <tr>
                <td>
                    <a href="?lat=<?= $item->getLatitude() ?>&lon=<?= $item->getLongitude() ?>"><?= $item->getAddress() ?></a>
                </td>
                <td><?= $item->getLatitude() ?></td>
                <td><?= $item->getLongitude() ?></td>
                <td><?= $item->getData()['Kind'] ?></td>
            </tr>
        <? endforeach; ?>
    </table>
</div>
<div id="map" class="map clearfix"></div>
<? endif; ?>
</body>
</html>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init() {
        myMap = new ymaps.Map("map", {
            center: [<?= $lat ?>, <?= $lon ?>],
            zoom: 7
        });

        myPlacemark = new ymaps.Placemark([<?= $lat ?>, <?= $lon ?>]);

        myMap.geoObjects.add(myPlacemark);
    }
</script>
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';

session_start();

function isPost()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function getParamPost($name)
{
    return array_key_exists($name, $_POST) ? $_POST[$name] : null;
}

function getParamSession($name)
{
    return array_key_exists($name, $_SESSION) ? $_SESSION[$name] : null;
}

function getParamGet($name)
{
    return array_key_exists($name, $_GET) ? $_GET[$name] : null;
}

function getApi()
{
    $api = new \Yandex\Geo\Api();
    return $api;
}

function findAddress($address, $limit = null)
{
    $api = getApi();

    // Можно искать по точке
    //$api->setPoint(30.284822, 59.943575);

    // Или можно икать по адресу
    $api->setQuery($address);

    // Настройка фильтров
    $api
        ->setLimit($limit)// кол-во результатов
        ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
        ->load();

    $response = $api->getResponse();

    return $response;
}